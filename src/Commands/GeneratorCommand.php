<?php

namespace Otls\Generator\Commands;

use Illuminate\Console\Command;
use Otls\Generator\Modules\Generator;
use Otls\Generator\Modules\Repository;
use Otls\Generator\Traits\CommandTrait;

class GeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'please:generate {generator} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate whatever files according to generator config file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');
        $generator = $this->argument('generator');

        if (!empty($name) && !empty($generator)) {
            $module = new Generator($generator, $name);
            $this->{$module->status}($module->message);
            return;
        }

        $this->error('You forgot something');
    }
}
