<?php

namespace Otls\Generator\Contracts;

interface ContentContract
{
    /**
     * Create content module
     */
    public function generate(string $name, string $namespace, string $ext = null, array $iface = []) :string;
}
