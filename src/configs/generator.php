<?php

return [
    'service' => [
        'path' => 'app/Http/Services',
        'generate_contract' => false,
        'contract_path' => null,
        'abstract' => [
            'path' => 'app/Http/Services',
            'name' => 'Service'
        ],
        'contracts' => []
    ],
    'repository'=> [
        'path' => 'app/Http/Repositories',
        'generate_contract' => true,
        'contract_path' => 'app/Http/Repositories/Contracts',
        'abstract' => [
            'path' => 'app/Http/Repositories',
            'name' => 'Repository'
        ],
        'contracts' => []
    ],
    'module'=> [
        'path' => 'app/Http/Modules',
        'generate_contract' => true,
        'contract_path' => null,
        'abstract' => [
            'path' => 'app/Http/Modules',
            'name' => 'Module'
        ],
        'contracts' => []
    ],
];
