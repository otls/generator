<?php

namespace Otls\Generator\Providers;

use Illuminate\Support\ServiceProvider;
use Otls\Generator\Commands\GeneratorCommand;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            GeneratorCommand::class
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../configs/generator.php' => config_path('generator.php'),
        ], 'otls-generator');
    }
}
