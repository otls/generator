<?php

namespace Otls\Generator\Modules;

use Exception;

class Generator
{
    private $config;
    private $tab = "\x20\x20\x20\x20";
    private $singleEOL = "\n";
    private $doubleEOL = "\n\n";
    private $name;
    private $abstract = null;
    private $iface = [];
    private $generator;
    public $status;
    public $message;

    public function __construct(string $generator, string $name)
    {
       if (config("generator.{$generator}")) {
           $this->config = config("generator.{$generator}");
       } else {
           $config = include(__DIR__ . "/../configs/generator.php");
           if (!isset($config[$generator])) {
               throw New Exception("Sorry.. {$generator} does not exist");
           }
           $this->config = $config[$generator];
       }
       $this->generator = $generator;
       $this->name = $name;

       $this->make();
    }

    /**
     * Begin making a file
     */
    public function make() :void
    {
        if ($this->config['abstract']) {
            $this->abstraction();
        }

        if ($this->config['generate_contract']) {
            $this->generateContract($this->name, $this->config['contract_suffix'] ?? "Contract");
        }

        $path = $this->config['path'];
        $nameArray = explode("/", $this->name);
        $name = end($nameArray);


        array_pop($nameArray);
        $nameArray = $nameArray ? implode("\\", $nameArray) : null;
        $namespace = $this->createNamespace($path, $nameArray);

        $filename = $path ."/" . $this->name . ".php";
        $content = $this->createContent($name, $namespace, $this->abstract, $this->iface);
        $this->createFile($filename, $content);
    }

    /**
     * Create a content of a class
     */
    public function createContent(string $name, string $namespace, string $ext = null, array $iface = null): string
    {
        $opener = "<?php\n\n";
        $namespace = "namespace {$namespace};\n\n";
        $class = "\nclass {$name}";

        $use = "";

        if ($ext) {
            $use .= "use {$ext};\n";
            $ext = $this->getNameFromNamespace($ext);
            $class .= " extends {$ext}";
        }

        if ($iface) {
            $class .= " implements";
            foreach ($iface as $k => $val) {
                $use .= "use {$val};\n";
                $val = $this->getNameFromNamespace($val);
                $class .= " {$val}";
            }
        }

        $content = "\n{\n\x20\x20\x20\x20#bebek apa yang kakinya dua?\n}\n";

        $moduleContent = $opener . $namespace . $use . $class . $content;
        return $moduleContent;
    }

    /**
     * Create base abstract class
     * It'll be skiped if the file already exists
     */
    public function abstraction() :void
    {
        $name = $this->config['abstract']['name'];
        $path = $this->config['abstract']['path'];

        $namespace = $this->createNamespace($path);
        $filename = $path . "/" . "{$name}.php";

        $this->abstract = $namespace . "\\" . $name;
        if (!file_exists($filename)) {
            $content = "<?php{$this->doubleEOL}namespace {$namespace};";
            $content .= "{$this->doubleEOL}abstract class {$name}{$this->singleEOL}";
            $content .= "{{$this->singleEOL}{$this->tab}#Bebek apa yang kakinya dua?{$this->singleEOL}}{$this->singleEOL}";

            $this->createFile($filename, $content);
        }
    }

    public function generateContract($name, $suffix = "Contract")
    {
        if (isset($this->config['contract_path']) && $this->config['contract_path']) {
            $path = $this->config['contract_path'];
        } else {
            $path = $this->config['path'];
        }
        $magicQuote = "#Bebek apa yang kakinya dua?";

        $namespace = $this->createNamespace($path);
        $className = $this->getName($name) . $suffix;
        $filename = "{$path}/{$className}.php";

        $this->iface[] = "{$namespace}\\{$className}";

        $content = "<?php\n\n";
        $content .= "namespace {$namespace};\n\n";
        $content .= "interface {$className}\n";

        $content .= "{\n{$this->tab}{$magicQuote}\n}\n";
        $this->createFile($filename, $content);
    }

    /**
     * Get name from user input
     */
    public function getName(string $name) :string
    {
        $nameArray = explode("/", $name);
        $name = end($nameArray);

        return ucfirst($name);
    }

    /**
     * Get class name from given namespace
     */
    public function getNameFromNamespace(string $namespace): string
    {
        $name = explode("\\", $namespace);
        return end($name);
    }

    /**
     * Create file/class
     */
    public function createFile(string $path, string $content) :void
    {
        $fullpath = base_path($path);
        $dir = base_path();

        $paths = explode("/", $path);
        array_pop($paths);

        foreach ($paths as $key => $path) {
            $dir .= "/{$path}";
            if (!file_exists($dir)) {
                mkdir($dir);
            }
        }

        $generator = ucfirst($this->generator);

        if (!file_exists($fullpath)) {
            file_put_contents($fullpath, $content);
            $this->message = "{$generator} successfully created: {$fullpath}";
            $this->status = "info";
        } else {
            $this->message = "{$generator} already exists: {$fullpath}";
            $this->status = "error";
        }
    }

    /**
     * Create namespace from base path config and filepath if exists
     */
    public function createNamespace(string $basePath, string $filePath = null) :string
    {
        $namespace = str_replace("/", "\\", $basePath);
        $namespace = ucfirst($namespace);

        if ($filePath) {
            $namespace .= "\\" . str_replace("/", "\\", $filePath);
        }
        return $namespace;
    }
}
