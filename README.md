# generator

It's a small Laravel package that could help to generate module files such as repository module, service module etc. It will generate files according to its configuration.


### Publish Config File
```
$ php artisan vendor:publish --tag=otls-generator
```
### Example config
```
[
    'service' => [
        'path' => 'app/Http/Services',
        'generate_contract' => false,
        'contract_path' => null,
        'abstract' => [
            'path' => 'app/Http/Services',
            'name' => 'Service'
        ],
        'contracts' => []
    ],
    'repository'=> [
        'path' => 'app/Http/Repositories',
        'generate_contract' => true,
        'contract_path' => 'app/Http/Repositories/Contracts',
        'abstract' => [
            'path' => 'app/Http/Repositories',
            'name' => 'Repository'
        ],
        'contracts' => []
    ],
];
```

### Usage
```
$ php artisan please:generate service AwesomeService
$ php artisan please:generate repository AwesomeRepo
```
